# tslib-seed
This is a basic seed for typescript library, includes tsconfig, tslint, package.json with all dependencies needed to build, lint and test the library. And gulp files with implementations for these tasks.

# requirements
For all gulp tasks to work, following global packages should be installed: typescript, tslint, nyc, gulp and mocha

# Features
1. Gulp tasks include building, linting, testing and measuring coverage for a typescript library to be created off this seed.
2. Running tests are using old gulp-mocha module so that breakpoint in visual studio would work. So far i have not found a way to do that with latest version of that module.
3. Along with building for latest target (ES2017), there are tasks to build the project for 2 old targets - ES5 and ES6. 
To import from one of those targets, use the corresponding suffix, like the following: import { maxBy } from '@vlr/array-tools/es5'
4. Gitlab CI script will do full test and coverage check on any pull request/merge request.
5. Gitlab CI script also will create version tag on master branch, and publish npm package.
   Version will be taken from npm if it is higher than previous tag, i.e. if it was set manually to higher value, 
   otherwize version from last set tag will be used with increment to patch version.


# Steps to create a typescript library from this seed

1. create a repository on gitlab
2. Settings -> General -> MergeRequests, set fast-forward and "only allow" settings
3. Settings -> CI/CD -> Environment Variables, set NPM_AUTH_TOKEN and GIT_PRIVATE_KEY
4. somewhere in your local folder, clone this repository and rename "origin" remote to seed"
5. set "origin" remote to your gitlab repository
5. push master branch to your repository, and then go to CI/CD and cancel the pipeline
7. create a branch for first version of your library.
8. in thatn branch update gitlab-ci.yml to have correct address of your gitlab repository
9. update your package.json and readme files


# Known issues
1. coverage does not work, due to nyc defect