import { series } from "gulp";
import { fullBuild } from "./build";
import { runTestCoverage } from "./parts/runTestCoverage";
import { runTestsEs5, runTestsEs6 } from "./parts";

export const fullTest = series(
  fullBuild,
  runTestsEs5, runTestsEs6,
  runTestCoverage,
);
