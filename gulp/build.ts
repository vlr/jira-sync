import { parallel, series } from "gulp";
import {
  compileTypescript, compileTypescriptEs6, compileTypescriptEs5, removeTempEs5Folder,
  removeBuildFolder, tslint, addEs5ImportTarget, addEs6ImportTarget, removeTempEs6Folder
} from "./parts";

const preparation = parallel(removeBuildFolder, tslint);
const latest = compileTypescript;
const es5 = series(compileTypescriptEs5, addEs5ImportTarget, removeTempEs5Folder);
const es6 = series(compileTypescriptEs6, addEs6ImportTarget, removeTempEs6Folder);

export const build = series(
  preparation,
  latest
);

export const fullBuild = series(
  preparation,
  parallel(latest, es5, es6)
);
