import * as gulp from "gulp";
import * as map from "gulp-transform";
import { allTargetFiles } from "../settings";
import { settings } from "../settings";

const regex = /((?:const|var) [^\s]* = require\(["|']\@vlr\/[^"\/]*)((?:\/[^"]*)?["|']\);)/g;

function replace(file: string, target: string): string {
  const result = file.replace(regex, (all, grp1, grp2) => grp1 + "/" + target + grp2);
  return result;
}

function addImportTarget(folder: string, target: string): NodeJS.ReadWriteStream {
  return gulp.src(allTargetFiles(folder + "temp"))
    .pipe(map("utf8", file => replace(file, target)))
    .pipe(gulp.dest(`./${folder}`));
}

export function addEs5ImportTarget(): NodeJS.ReadWriteStream {
  return addImportTarget(settings.buildEs5, "es5");
}

export function addEs6ImportTarget(): NodeJS.ReadWriteStream {
  return addImportTarget(settings.buildEs6, "es6");
}
